﻿using UnityEngine;
using TouchControlsKit;

public class PlayerAttack : MonoBehaviour
{
    public FirstPersonAIO fpsController;

    public int damagePerAttack = 20;                  // The damage inflicted by each bullet.
    public float timeBetweenAttacks = 0.15f;        // The time between each shot.
    float timer;                                    // A timer to determine when to fire.
    
    public AudioSource attackAudio;                           // Reference to the audio source.
    public AudioClip swordClip;
    public AudioClip chopClip;

    float effectsDisplayTime = 0.2f;                // The proportion of the timeBetweenBullets that the effects will display for.

    public GameObject axe;     //reference to the axe gameobject
    Animator axeAnim;
    PlayerFrontCheck playerFrontCheck;


    private void Awake()
    {
        axeAnim = axe.GetComponent<Animator>();
        fpsController = GetComponent<FirstPersonAIO>();
        playerFrontCheck = GetComponent<PlayerFrontCheck>();
    }

    void Update()
    {
        // Add the time since Update was last called to the timer.
        timer += Time.deltaTime;
        
        if (fpsController.enableTouchControls)
        {
            if (TCKInput.GetAction("fireBtn", EActionEvent.Down) && timer >= timeBetweenAttacks && axe.activeInHierarchy)
            {
                Attack();
            }
        }
        else
        {
            if (Input.GetButtonDown("Attack") && timer >= timeBetweenAttacks && axe.activeInHierarchy)
            {
                Attack();
            }
        }
    }




    void Attack()
    {
        // Reset the timer.
        timer = 0f;

        if (axeAnim)
            axeAnim.SetTrigger("Attack");

        attackAudio.Play();

        if (playerFrontCheck.enemy)
        {
            attackAudio.PlayOneShot(swordClip);
            playerFrontCheck.enemy.TakeDamage(damagePerAttack);
        }   
        
        if (playerFrontCheck.tree)
        {
            attackAudio.PlayOneShot(chopClip);
            playerFrontCheck.tree.TakeDamage(damagePerAttack);
        }
    }
}