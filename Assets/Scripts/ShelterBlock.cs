﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ShelterBlock : MonoBehaviour
{
    public Material completeMaterial;
    public Material ghostMaterial;

    [HideInInspector]
    public bool complete;

    public void Build()
    {        
        GetComponent<MeshRenderer>().material = completeMaterial;
        GetComponent<Collider>().isTrigger = false;
        GetComponent<NavMeshObstacle>().enabled = true;
        complete = true;
    }
}
