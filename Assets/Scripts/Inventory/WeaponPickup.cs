﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPickup : MonoBehaviour
{
    public GameObject playerAxe;
    public GameObject uiAxe;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            playerAxe.SetActive(true);
            uiAxe.SetActive(true);
            Destroy(gameObject);
        }                    
    }
}
