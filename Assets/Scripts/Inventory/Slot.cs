﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slot : MonoBehaviour
{
    public bool isOccupied;
    private Inventory inventory;
    
    private void Start()
    {

        inventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
    }

    public void Cross()
    {

        foreach (Transform child in transform)
        {
            //child.GetComponent<Spawn>().SpawnItem();          //лучше назвать его Dropper.DropItem()
            Destroy(child.gameObject);
        }
    }
}
