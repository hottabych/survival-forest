﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Скрипт назначается на дерево. Из него выпадают кусочки древесины
/// </summary>
public class Tree : MonoBehaviour
{
    public int startingHealth = 100;           
    public int currentHealth;

    public GameObject loot;
    public Transform lootSpawnPos;

    new AudioSource audio;
    public AudioClip chopClip, destroyTreeClip;

    private PlayerFrontCheck playerFrontCheck;


    private void Awake()
    {
        audio = GetComponent<AudioSource>();
        currentHealth = startingHealth;
        playerFrontCheck = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerFrontCheck>();
    }

    public void TakeDamage(int amount)
    {
        if (chopClip)
            audio.PlayOneShot(chopClip);        //уже сделал в PlayerAttack
        
        currentHealth -= amount;

        if (currentHealth <= 0)
        {
            DropLoot();
        }
    }

    public void DropLoot()
    {
        if (destroyTreeClip)
            audio.PlayOneShot(destroyTreeClip);

        //дропнуть кусочек древесины
        Instantiate(loot, lootSpawnPos.position, Quaternion.identity);

        //уведомить игрока, что дерево пропало
        playerFrontCheck.OnTriggerDisappear(this.transform);

        //удалить дерево
        Destroy(gameObject);
    }
}
