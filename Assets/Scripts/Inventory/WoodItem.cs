﻿using UnityEngine;

public class WoodItem : MonoBehaviour
{
    PlayerFrontCheck playerFrontCheck;

    private void Awake()
    {
        playerFrontCheck = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerFrontCheck>();
    }

    public void Use()
    {
        if (playerFrontCheck.shelterBlock)
        {
            if (!playerFrontCheck.shelterBlock.complete)
            {
                //sound
                var sound = transform.GetChild(0).gameObject;
                sound.GetComponent<AudioSource>().Play();
                Destroy(sound, 2f);
                transform.DetachChildren();

                playerFrontCheck.shelterBlock.Build();
                Destroy(gameObject);
            }                
        }
    }
}
