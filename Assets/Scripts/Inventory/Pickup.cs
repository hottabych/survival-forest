﻿using UnityEngine;


/// <summary>
/// этот скрипт вешается на объекты, которые можно подобрать
/// например, на кусочек древесины
/// </summary>
public class Pickup : MonoBehaviour
{
    private Inventory inventory;
    public GameObject itemButton;       //кнопка для слота инвентаря

    private void Start()
    {
        inventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            for (int i = 0; i < inventory.slots.Length; i++)
            {
                Slot currentSlot = inventory.slots[i].GetComponent<Slot>();

                //очищаем освободившиеся слоты
                if (inventory.slots[i].transform.childCount <= 0)
                {
                    currentSlot.isOccupied = false;
                }

                //если слот пустой -- заполняем его
                if (currentSlot.isOccupied == false)
                {
                    currentSlot.isOccupied = true;

                    //pickup sound
                    var sound = transform.GetChild(0).gameObject;     
                    sound.GetComponent<AudioSource>().Play();
                    Destroy(sound, 2f);
                    transform.DetachChildren();

                    Instantiate(itemButton, inventory.slots[i].transform, false); // spawn the button so that the player can interact with it
                    Destroy(gameObject);
                    break;
                }
            }
        }

    }
}
