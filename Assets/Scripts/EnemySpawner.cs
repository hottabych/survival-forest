﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemy;

    public float startingDelayMin = 20, startingDelayMax = 30, spawnDelayMin = 20, spawnDelayMax = 30;

    IEnumerator Start()
    {
        yield return new WaitForSeconds(Random.Range(startingDelayMin, startingDelayMax));            

        while (true)
        {
            Instantiate(enemy, transform.position, Quaternion.identity);
            yield return new WaitForSeconds(Random.Range(spawnDelayMin, spawnDelayMax));        
        }
    }


}
