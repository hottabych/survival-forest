﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFrontCheck : MonoBehaviour
{
    public EnemyHealth enemy;          //враг в триггере поражения
    public Tree tree;                  //дерево в триггере
    public ShelterBlock shelterBlock;  //место для постройки блока в триггере

    void OnTriggerEnter(Collider other)
    {
        // If the entering collider is an enemy...
        if (other.CompareTag("Enemy"))
        {
            enemy = other.GetComponent<EnemyHealth>();
        }

        if (other.CompareTag("Tree"))
        {
            tree = other.GetComponent<Tree>();
        }

        if (other.CompareTag("Shelter"))
        {
            shelterBlock = other.GetComponent<ShelterBlock>();
        }
    }


    void OnTriggerExit(Collider other)
    {
        // If the exiting collider is an enemy...
        if (other.CompareTag("Enemy"))
        {
            // ... an enemy is no longer in range.            
            enemy = null;
        }

        if (other.GetComponent("Tree"))
        {
            tree = null;
        }

        if (other.CompareTag("Shelter"))
        {
            shelterBlock = null;
        }
    }


    //event handler
    public void OnTriggerDisappear(Transform sender)
    {
        if (sender.tag == "Enemy") enemy = null;
        if (sender.tag == "Tree") tree = null;
        if (sender.tag == "Shelter") shelterBlock = null;
    }
}
